NAME
	prosody-migrator - Migrate data between Prosody data stores

SYNOPSIS
	prosody-migrator [OPTIONS] [SOURCE_STORE DESTINATION_STORE]

DESCRIPTION
	prosody-migrator is used to move data (user accounts, rosters, vcards, etc.)
	between different data stores. For example this allows you to migrate data from
	a file-based store to an SQL database, and vice-versa.
	
	Note: the migrator assumes that the destination store is empty. Existing data
	that conflicts with data in the source store will be overwritten!

OPTIONS
	--config=FILENAME  
		Specify an alternative config file to use. The default is
		/etc/prosody/migrator.cfg.lua.
	
	SOURCE_STORE  
		The name of the store (defined in the config file) to migrate data from.
		Defaults to 'input'.

	DESTINATION_STORE  
		The name of the store (also defined in the config file) to migrate data to.
		Defaults to 'output'.

FILES
	/etc/prosody/migrator.cfg.lua  
		The migrator config file.

SEE ALSO
	prosody(8), prosodyctl(8)

AUTHOR
	Matthew Wild <mwild1@gmail.com>
